<?php
/**
 * Plugin Name: HIOICE Showcase Custom Post Type
 * Description: Adds a Custom Post Type for the 'Maxmedia 2017' theme, used to show clients and projects. 
 * Author: Dan Nagle
 * Text Domain: hioice-showcase
 * Domain Path: /languages/
 * Version: 1.0.2
 * License: GPLv2
 * Bitbucket Plugin URI: https://bitbucket.org/dnagle/hioice-showcase-cpt
 *
 * @package WordPress
 * @author Dan Nagle
 */

if ( !defined( 'HIOICE_SC_CPT_VERSION' ) ) {
	define( 'HIOICE_SC_CPT_VERSION', '1.0.2' ); // Plugin version
}
if ( !defined( 'HIOICE_SC_CPT_DIR' ) ) {
	define( 'HIOICE_SC_CPT_DIR', dirname( __FILE__ ) ); // Plugin directory
}
if ( !defined( 'HIOICE_SC_CPT_URL' ) ) {
	define( 'HIOICE_SC_CPT_URL', plugin_dir_url( __FILE__ ) ); // Plugin url
}
if ( !defined( 'HIOICE_SC_CPT_DOMAIN' ) ) {
	define( 'HIOICE_SC_CPT_DOMAIN', 'hioice-showcase' ); // Text Domain
}

/**
 * Load Text Domain
 * This gets the plugin ready for translation
 * 
 * @package Showcase Custom Post Type
 * @since 1.0.0
 */
function hioice_sc_cpt_load_textdomain() {
	load_plugin_textdomain( HIOICE_SC_CPT_DOMAIN, false, HIOICE_SC_CPT_DIR . '/languages/' );
}
add_action('plugins_loaded', 'hioice_sc_cpt_load_textdomain');


function hioice_create_showcase_post_type() {

	register_post_type( 'hioice_showcase',
		array(
			'labels' => array(
				'name' => __( 'Showcase', HIOICE_SC_CPT_DOMAIN ),
				'singular_name' => __( 'Showcase', HIOICE_SC_CPT_DOMAIN ),
				'add_new' => __( 'Add New', HIOICE_SC_CPT_DOMAIN ),
				'add_new_item' => __( 'Add New Showcase', HIOICE_SC_CPT_DOMAIN ),
				'edit' => __( 'Edit', HIOICE_SC_CPT_DOMAIN ),
				'edit_item' => __( 'Edit Showcase', HIOICE_SC_CPT_DOMAIN ),
				'new_item' => __( 'New Showcase', HIOICE_SC_CPT_DOMAIN ),
				'view' => __( 'View', HIOICE_SC_CPT_DOMAIN ),
				'view_item' => __( 'View Showcase', HIOICE_SC_CPT_DOMAIN ),
				'search_items' => __( 'Search Showcases', HIOICE_SC_CPT_DOMAIN ),
				'not_found' => __( 'No Showcases found', HIOICE_SC_CPT_DOMAIN ),
				'not_found_in_trash' => __( 'No Showcases found in Trash', HIOICE_SC_CPT_DOMAIN ),
				'parent' => __( 'Parent Showcase', HIOICE_SC_CPT_DOMAIN ),
			),
			'public' => false,
			'show_ui' => true,
			'menu_position' => 20,
			'supports' => array(
				'title', 'editor', 'thumbnail'
			),
			'taxonomies' => array( '' ),
			'menu_icon' => plugins_url( 'images/showcase-icon.png', __FILE__ ),
			'has_archive' => false,
			'publicly_queryable' => false,
			'exclude_from_search' => true,
		)
	);

}
add_action( 'init', 'hioice_create_showcase_post_type' );


function hioice_showcase_shortcode( $atts, $content=null ) {

	$defaults = array( 'post_ids' => '-1' );

	$output = '';
	
	extract( shortcode_atts($defaults, $atts) );

	$query_params = array(
		'post_type' => 'hioice_showcase',
		'post_status' => array('publish'),
 		'ignore_sticky_posts' => 1,
		'orderby' => 'ID',
		'order' => 'ASC',
		'suppress_filters' => true,
	);

	$box_ids = explode( ',', $post_ids );

	if ( count($box_ids) > 0 && $box_ids[0] > 0 ) {
		$query_params['post__in'] = $box_ids;
	}

	$showcase_query = new WP_Query( $query_params );

	$odd_or_even = 'even';

	if ( $showcase_query->have_posts() ) {

		$count = $showcase_query->post_count;

		ob_start();
?>
<div class="showcase-grid showcase-gallery" data-panels="<? echo $count; ?>">
<?php
		while ( $showcase_query->have_posts() ) {

			$showcase_query->the_post();

			$odd_or_even = ($odd_or_even == 'odd' ? 'even' : 'odd');

			$showcase_title = trim( get_the_title( get_the_ID() ) );

			$image = get_field('hioice_showcase_logo');
			$image_size = '600w_15';
			if ( $image ) {
				$logo = wp_get_attachment_image( $image['id'], $image_size );
			}
			else {
				$logo = '<h3>' . $showcase_title . '</h3>';
			}

			$link = get_field('hioice_showcase_link');
			if ( $link ) {
				$open_tag = '<a class="showcase-slide" href="' . $link['url'] . '" target="' . $link['target'] . '" title="' . $link['title'] . '" >';
				$close_tag = '</a>';
			}
			else {
				$open_tag = '<div class="showcase-slide">';
				$close_tag = '</div>';
			}

?>
	<div class="showcase-panel is-panel">
		<?php echo get_the_post_thumbnail( get_the_ID() ); ?>
		<?php echo $open_tag; ?>
			<div class="showcase-logo">
				<?php echo $logo; ?>
			</div>
			<div class="showcase-hover <?php echo $odd_or_even; ?>">
				<div class="showcase-content">
					<?php echo $logo; ?>
					<div class="showcase-text">
						<p><?php echo get_the_content( get_the_ID() ); ?></p>
					</div>
				</div>
			</div>
		<?php echo $close_tag; ?>
	</div>
<?php
		}

		wp_reset_postdata();
?>
	<div class="showcase-panel is-padding" style="display:none;">
		<img src="<?php echo plugins_url( 'images/padding-pattern.jpg', __FILE__ ); ?>" alt="" />
		<div class="showcase-slide"></div>
	</div>
</div>
<?php
		$output = ob_get_contents();
		ob_end_clean();
	}

	return $output;
}


/*
 * Function for registering the [hioice-showcase-grid] shortcode.
 */
function hioice_showcase_register_shortcodes() {
	add_shortcode( 'hioice-showcase-grid', 'hioice_showcase_shortcode' );
}
/* Register shortcodes in 'init'. */
add_action( 'init', 'hioice_showcase_register_shortcodes' );
